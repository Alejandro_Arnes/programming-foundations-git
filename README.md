# Programming Foundations: Intro to Git

Exercise files for the **Programming Foundations: Intro to Git** course on [LinkedIn Learning](https://www.linkedin.com/learning/instructors/christina-truong?u=2125562).

Created by [Christina Truong](http://christinatruong.com)

Find me on [Twitter](http://twitter.com/christinatruong) or [Instagram](http://instagram.com/christina.is.online)!
